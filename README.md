# TP Web-Serveur

## Compilation

Vous pouvez démarrer avec la commande `./gradlew run`.

+ d'infos sur https://unicorn.artheriom.fr/

## Auteurs

* **Nourrane MALLEPEYRE** _alias_ [@lomechoud](https://gitlab.isima.fr/nomallepey)
* **Cynthia LOURENCO** _alias_ [@cylourenco](https://gitlab.isima.fr/users/cylourenco)
* **Louane MECHOUD** _alias_ [@lomechoud](https://gitlab.isima.fr/users/lomechoud)
